const fs = require('fs');
const os = require('os');
const express = require('express');
const cors = require('cors');
const csrf = require('csurf');
const cookieParser = require('cookie-parser');
const multer = require('multer');
const sanitize = require('sanitize-filename');

const app = express();
const port = 3000;
const maxFileSize = 1e+7; // 10 MB

app.use(cors({
  origin: 'http://localhost:8080',
  exposedHeaders: 'XSRF-TOKEN',
  credentials: true,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

// setup route middlewares
const csrfProtection = csrf({ cookie: true });
// parse cookies. We need this because "cookie" is true in csrfProtection
app.use(cookieParser());
app.use(csrfProtection);

// FIXME: Basic session, add a proper solution like express-session + Redis/MongoDB
app.set('session', new Map());

/*
 * Filter passed to multer uploader to allow only certain types
 */
const fileFilter = (req, file, cb) => {
  let acceptFile = false;
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    acceptFile = true;
  } else {
    console.error('File with forbidden MIME type %s, denied', file.mimetype);
  }
  cb(null, acceptFile);
};

const getFolder = (token) => `${os.tmpdir()}/rum_${token.substr(0, 12)}/`;

/*
 * Set up disk storage object needed for multer uploader.
 * We pass destination and filename functions.
 */
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // We create a folder with the csrf token, that belongs to a 'session'
    // Same user should always send the same csrf token
    const fullPath = req.headers['csrf-token']
      ? getFolder(req.headers['csrf-token'])
      : '';
    fs.access(fullPath, fs.constants.F_OK, (err) => {
      // If folder doesn't exist, create it
      if (err) {
        fs.mkdirSync(fullPath);
      }
      cb(null, fullPath);
    });
  },
  filename: (req, file, cb) => {
    cb(null, sanitize(file.originalname));
  },
});

const upload = multer({
  storage,
  fileFilter,
  limits: {
    fileSize: maxFileSize,
    files: 1,
    fields: 1,
  },
});

/*
 * Get list of downloads
 */
app.get('/downloads', (req, res) => {
  const { headers } = req;
  let token = headers['csrf-token'];
  const session = app.get('session');
  let files = new Map();

  if (!token) {
    // Add CSRF token to headers
    token = req.csrfToken();
    res.append('XSRF-TOKEN', token);
  } else if (session.has(token)) {
    files = session.get(token);
  }
  session.set(token, files);
  app.set('session', session);
  res.json({
    documents: Array.from(files.values()),
  });
});


/*
 * Delete a file
 */
app.delete('/downloads/delete/:name', csrfProtection, (req, res) => {
  const { headers, params } = req;
  const token = headers['csrf-token'];
  const session = app.get('session');
  const files = session.get(token);

  if (files.has(params.name)) {
    const file = getFolder(token) + params.name;
    fs.unlinkSync(file);
    files.delete(params.name);
    session.set(token, files);
    app.set('session', session);
    return res.send('OK');
  } else {
    res.status(404);
    return res.send({ error: 'File not found' });
  }
});

const randomId = () => Math.random().toString(36).substr(2, 5);

/*
 * Post endpoint for file upload (multipart/form-data). Expects a form entry with same name.
 */
app.post('/downloads/upload', csrfProtection, upload.single('some_unique_id'),
  (req, res) => {
    const { file, headers } = req;
    if (!file) {
      res.status(415);
      return res.send({ error: 'Unsupported file format or invalid' });
    }

    const token = headers['csrf-token'];
    const session = app.get('session');
    if (session.has(token)) {
      const files = session.get(token);
      if (!files.has(file.filename)) {
        const newFile = {
          id: randomId(),
          name: file.filename,
          mimetype: file.mimeType,
          size: file.size,
        };
        files.set(file.filename, newFile);
        session.set(token, files);
        app.set('session', session);
        return res.json(newFile);
      } else {
        res.status(409);
        return res.send({ error: 'Duplicated file!' });
      }
    }

    return res.send(file);
  },
);


// Optionally we can serve the frontend from here too
app.use(express.static('client/public'));
app.get('/', (req, res, next) => {
  res.set('Content-Type', 'text/html');
  fs.readFile('client/public/index.html', (err, fileBuffer) => {
    if (err) {
      return next(err); // Tell express to handle errors
    }
    return res.send(fileBuffer.toString());
  });
});

app.listen(port, () => console.log(`API server listening on port ${port}!`));
