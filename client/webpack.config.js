const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
  entry: './src/index.js',
  output: {
    path: `${__dirname}/public/dist/`,
    publicPath: '/dist',
    filename: 'bundle.js',
  },
  devServer: {
    contentBase: `${__dirname}/public`,
    hot: true,
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: ['babel-loader'],
    },
    {
      test: /\.scss$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: true,
            sourceMap: isDevelopment,
            importLoaders: 2,
          },
        },
        'sass-loader'],
    }],
  },
};
