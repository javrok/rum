import React from 'react';
import PropTypes from 'prop-types';
import style from './_button.scss';

const Button = ({ children, onClick, skin }) => {
  const className = `${style.button} ${skin ? style[`button--${skin}`] : ''}`;
  return (
    <button
      type="button"
      className={className}
      onClick={onClick}
    >
      { children }
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
  skin: PropTypes.string,
};

Button.defaultProps = {
  children: null,
  skin: null,
};

export default Button;
