import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import style from './_file-uploader.scss';
import Button from '../Button';
import ErrorMessage from '../ErrorMessage';

const endpoint = 'http://localhost:3000/downloads/upload';

const FileUploader = ({ id, xsrfToken, fileUploaded }) => {
  const labelRef = useRef(null);
  const [fileSelected, setFileSelected] = useState(null);
  const [error, setError] = useState(null);

  const onBtnClick = () => {
    labelRef.current.click();
  };

  const onFileSelect = (evt) => {
    const { target } = evt;
    if (target.files && target.files.length) {
      setFileSelected(target.files[0]);
    }
  };

  const handleUpload = (evt) => {
    evt.preventDefault();
    const data = new FormData();
    data.append(id, fileSelected, fileSelected.name);
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        'CSRF-Token': xsrfToken,
      },
      withCredentials: true,
    };
    axios.post(endpoint, data, config).then((res) => {
      setFileSelected(false);
      fileUploaded && fileUploaded(res.data);
    })
      .catch((err) => {
        const msg = err.response.data && err.response.data.error;
        setError(msg ? msg : err.message);
      });
  };

  return (
    <form className={style.uploader}>
      <ErrorMessage message={error} />
      <label htmlFor={id} ref={labelRef}>
        { fileSelected && (
          <Button onClick={onBtnClick}>Change</Button>
        )}
        { fileSelected && <span className={style.uploader__filename}>{ fileSelected.name }</span> }
        <Button onClick={fileSelected ? handleUpload : onBtnClick} skin="blue">
          { fileSelected ? 'Confirm' : 'Upload' }
        </Button>
        <input
          id={id}
          type="file"
          name={id}
          className={style.uploader__input}
          onChange={onFileSelect}
        />
      </label>
    </form>
  );
};

FileUploader.propTypes = {
  id: PropTypes.string.isRequired,
  xsrfToken: PropTypes.string,
  fileUploaded: PropTypes.func,
};

FileUploader.defaultProps = {
  xsrfToken: null,
  fileUploaded: null,
};

export default FileUploader;
