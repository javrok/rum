import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import style from './_error-message.scss';

const ErrorMessage = ({ message }) => {
  const [hidden, setHidden] = useState(true);
  const className = `${style['error-message']} ${hidden ? style.hidden : ''}`;

  useEffect(() => {
    if (message) {
      setHidden(false);
      setTimeout(() => setHidden(true), 5000);
    }
  }, [message]);

  return (
    <div className={className}>
      { message }
    </div>
  );
};

ErrorMessage.propTypes = {
  message: PropTypes.string,
};

ErrorMessage.defaultProps = {
  message: '',
};

export default ErrorMessage;
