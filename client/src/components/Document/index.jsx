import React from 'react';
import PropTypes from 'prop-types';

import { formatFilesize } from '../../util';
import style from './_document.scss';
import Button from '../Button';

const Document = ({ title, size, onDelete }) => (
  <div className={style.document}>
    <h3>{title}</h3>
    <div className={style.document__footer}>
      <div className={style.document__weight}>{formatFilesize(size)}</div>
      <Button onClick={onDelete}>
        Delete
      </Button>
    </div>
  </div>
);

Document.propTypes = {
  title: PropTypes.string.isRequired,
  size: PropTypes.number, // in Kilobytes
  onDelete: PropTypes.func.isRequired,
};

Document.defaultProps = {
  size: 0,
};

export default Document;
