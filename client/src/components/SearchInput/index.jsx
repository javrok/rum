import React from 'react';
import PropTypes from 'prop-types';

import style from './_search-input.scss';

const SearchInput = ({ onSearch }) => {
  const onChange = (evt) => {
    onSearch && onSearch(evt.target.value);
  };

  return (
    <div className={style.search}>
      <input
        className={style.search__input}
        type="search"
        placeholder="Search documents.."
        onChange={onChange}
      />
    </div>
  );
};

SearchInput.propTypes = {
  onSearch: PropTypes.func,
};

SearchInput.defaultProps = {
  onSearch: null,
};

export default SearchInput;
