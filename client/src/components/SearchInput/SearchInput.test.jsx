import React from 'react';
import { shallow } from 'enzyme';
import Document from '.';

describe('My Test Suite', () => {
  it('My Test Case', () => {
    const wrapper = shallow(<Document title="test" />);
    expect(wrapper.text()).toBe('test');
  });
});
