import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Document from './components/Document';
import SearchInput from './components/SearchInput';
import FileUploader from './components/FileUploader';
import style from './style/app.scss';
import { setOnUnloadWarning } from './util';

const endpoint = 'http://localhost:3000/downloads/';

const fetchDownloads = async (token = '') => (
  axios(endpoint, {
    withCredentials: true,
    headers: {
      'csrf-token': token,
    },
    responseType: 'json',
  })
);

const deleteDownload = async (name, token = '') => (
  axios.delete(`${endpoint}delete/${name}`, {
    withCredentials: true,
    headers: {
      'CSRF-Token': token,
    },
    responseType: 'json',
  })
);

const App = () => {
  const [response, setResponse] = useState({ documents: [] });
  const [xsrfToken, setXsrfToken] = useState('');
  const [query, setQuery] = useState('');
  const [deleted, setDeleted] = useState(null);
  const { documents } = response;

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchDownloads();
      setXsrfToken(result.headers['xsrf-token']);
      setResponse(result.data);
    };
    fetchData();
    setOnUnloadWarning();
  }, []);

  useEffect(() => {
    const deleteDoc = async () => {
      if (deleted) {
        await deleteDownload(deleted, xsrfToken);
        setResponse({
          documents: [
            ...response.documents.filter((doc) => doc.name !== deleted),
          ],
        });
      }
    };
    deleteDoc();
  }, [deleted]);

  const fileUploaded = (newFile) => {
    setResponse({
      documents: [
        newFile,
        ...response.documents,
      ],
    });
  };

  const downloads = documents && documents.length
    ? documents.filter((doc) => doc.name.toLowerCase().includes(query))
    : null;

  return (
    <div className={style.rum}>
      <h1>React Upload Manager</h1>
      <div className={style.rum__header}>
        <SearchInput onSearch={(keyword) => setQuery(keyword.toLowerCase())} />
        <FileUploader id="some_unique_id" xsrfToken={xsrfToken} fileUploaded={fileUploaded} />
      </div>
      <div className={style.rum__docslist}>
        {downloads && downloads.length
          ? downloads.map((doc) => (
            <Document key={doc.id} title={doc.name} size={doc.size} onDelete={() => setDeleted(doc.name)} />
          ))
          : <p>No results</p>}
      </div>
    </div>
  );
};

export default App;
