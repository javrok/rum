
/*
 * @param {number} size of the file in bytes
 * @returns {string} human friendly file size
 */
export const formatFilesize = (size) => {
  switch (true) {
    case size > 1e+9:
      return `${Math.round(size / 1e+9)}GB`;
    case size > 1e+6:
      return `${Math.round(size / 1e+6)}MB`;
    case size > 1000:
      return `${Math.round(size / 1000)}KB`;
    default:
      return '<1KB';
  }
};

export const getCookie = (name) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) {
    return parts.pop().split(';').shift();
  }
  return null;
};

export const setOnUnloadWarning = () => {
  window.onbeforeunload = () => 'If you close the browser window, you will lose all your changes. Are you sure?';
};
