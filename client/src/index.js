import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import App from './App';

ReactDOM.render(
  React.createElement(App),
  document.getElementById('root'),
);
