## Getting started

### Installation
```
npm install
```

### Running
To run both client and server locally:
```
npm start
```

To run them separatedly:

##### Server
```
npm run start:server
```
##### Client (with webpack-dev-server):
```
npm run start:client
```

To create a client bundle for deployment:
```
npm run build:client
```
This will generate all necessary files in client/public folder.
You can either put client build in a static server and API code in a node server, 
or you can also serve static content from the same server (didn't test it completely)


### Security
An anonymous public form creates some security concerns, specially when users can upload several files.
Measures taken for security:
- Added protection against CSRF, by using a token sent on first call. 
  All subsequent calls from the user should have te token (cookie and headers). Token gets renewed every session.
- Against DoS attacks, limit to 1 file per time, max 10MB, onlye images, with multer options. The rest should
be taken care of at infrastructure level. 
- Sanitized filename before saving. 
- XSS very unlikely since React escapes all string props, I don't use dangerouslySetInnerHTML,
 and there's no props controlled by user.

Measures that have not been addressed
- TLS encryption over HTTPS (can't without a proper server)
- Proper control over disk usage and possible attacks through filesystem.


### Improvements

- Definitively Unit testing, I didn't have time to do it, even though I set up Jest and enzyme on the project.
 I know it was a requirement, but the application took longer than expected.
- Proper session management, like express-session + Redis/MongoDB for instance.
- Pagination
- Download files (it's pretty obvious, but wasn't mentioned on the document).
- Typescript (I prefer it over PropTypes and JSDocs)


### Libraries

#### Server
- Expressjs for REST endpoints (with cors)
- Multer for file upload
- csurf for CSRF protection

#### Client

- Axios for API calls
- Webpack + Babel
- Sass
- Eslint (with Airbnb standards)

## Downloads API


```
### GET /downloads
// Gets a list of existing downloads for current user.
// Needs to include csrf token to retrieve existing ones; otherwise provides a new one with an empty list.
// Returns JSON array with file objects:
{
  id: string,
  name: string,
  mimetype: string,
  size: number, // in bytes
}
```


```
#### Upload file 
POST /downloads/upload
// Entrypoint for file uploading through multipart/form-data 
// Responds with JSON body with the new file object or error message if format is not allowed.
```

```
#### Delete file 
DELETE /downloads/delete/:name
// Delete a file by passing _:name_ as parameter (only filename, no path)
